FROM node:12.2.0

# install angular-cli as node user
RUN chown -R node:node /usr/local/lib/node_modules \
    && chown -R node:node /usr/local/bin
    
USER node
RUN npm install -g @angular/cli@8.3.21 fail for no permission

# return to root (is this needed ?)
USER root
