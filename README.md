# Docker Images

Docker images based used as intermediate images usually to build the project.  


## registry.gitlab.com/alessandro.piccione/docker-images/angular

Used to build Angular applications.

- NodeJs: 12.2.0  
- Angular CLI: 8.3.21
